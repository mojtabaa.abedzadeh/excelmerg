﻿using System.Windows;

namespace ExcelMerg.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnLoadTaminWindow_Click(object sender, RoutedEventArgs e)
        {
            new TaminWindow().ShowDialog();
        }

        private void BtnLoadMosalahWindow_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
