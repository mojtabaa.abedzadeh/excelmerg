﻿using ExcelMerg.Classes;
using Microsoft.Win32;
using System;
using System.Data;
using System.Globalization;
using System.Windows;

namespace ExcelMerg.Windows
{
    /// <summary>
    /// Interaction logic for TaminWindow.xaml
    /// </summary>
    public partial class TaminWindow : Window
    {
        string today = "";
        PersianCalendar pc = new PersianCalendar();
        DataTable dtKeshvari = null;
        DataTable dtMain = null;
        DataTable dtDate = null;
        private int G, F, E, D;
        string path = string.Empty;


        public TaminWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            today = pc.GetYear(DateTime.Now).ToString("0000/") +
                   pc.GetMonth(DateTime.Now).ToString("00/") +
                   pc.GetDayOfMonth(DateTime.Now).ToString("00");
        }

        private void BtnBrowserBaseFile_Click(object sender, RoutedEventArgs e)
        {
            dtKeshvari = new DataTable();
            dtKeshvari = CSConvertExcelToDataTable.GetDataTable(Browse(), true);
        }

        private void BtnBrowserDate_Click(object sender, RoutedEventArgs e)
        {
            dtDate = new DataTable();
            dtDate = CSConvertExcelToDataTable.GetDataTable(Browse(), true);
        }

        private void BtnGetPercent_Click(object sender, RoutedEventArgs e)
        {
            if (dtKeshvari != null && dtDate != null)
            {
                CalculationTaminPercent();
            }
            else
            {
                MessageBox.Show("لطفا ابتدا فایل‌های اکسل را وارد نمائید!");
            }
        }

        private void BtnGetExcel_Click(object sender, RoutedEventArgs e)
        {
            if (dtKeshvari != null && dtDate != null)
            {
                DataTable dtMerged = new DataTable();
                DataView view = (DataView)DgShowMergedData.ItemsSource;
                dtMerged = view.Table.Clone();

                foreach (DataRowView dataRowView in view)
                {
                    dtMerged.ImportRow(dataRowView.Row);
                }

                SaveFileDialog saveFileDialog = new SaveFileDialog();

                if (saveFileDialog.ShowDialog() == true)
                {
                    CSConvertExcelToDataTable.ExportToExcel(dtMerged, saveFileDialog.FileName, true);
                }
            }
            else
            {
                MessageBox.Show("لطفا ابتدا فایل‌های اکسل را وارد نمائید!");
            }
        }

        private void CalculationTaminPercent()
        {
            if (dtKeshvari != null && dtDate != null)
            {
                float percent = 0;
                dtMain = new DataTable();
                AddGridColumns(dtMain);

                for (int i = 0; i < dtKeshvari.Rows.Count; i++)
                {
                    string[] Data = new string[18];
                    string code = string.Empty;

                    for (int j = 0; j <= dtKeshvari.Columns.Count + 2; j++)
                    {
                        switch (j)
                        {
                            case 0:
                                if (i == 0)
                                    Data[0] = "نام دارو";
                                else
                                    Data[0] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 1:
                                if (i == 0)
                                    Data[1] = "کد دارو";
                                else
                                {
                                    Data[1] = dtKeshvari.Rows[i][j].ToString();
                                    Data[12] = dtDate.Rows[i][3].ToString();
                                    Data[16] = dtDate.Rows[i][1].ToString();
                                    Data[17] = dtDate.Rows[i][2].ToString();
                                }
                                break;
                            case 2:
                                if (i == 0)
                                    Data[2] = "صرفا وبی";
                                else
                                    Data[2] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 3:
                                if (i == 0)
                                    Data[3] = "سقف تجویز";
                                else
                                {
                                    D = int.Parse(dtKeshvari.Rows[i][j].ToString());
                                    Data[6] = dtKeshvari.Rows[i][j].ToString();
                                }
                                break;
                            case 4:
                                if (i == 0)
                                    Data[4] = "نیاز به اخذ مدرک";
                                else
                                {
                                    E = int.Parse(dtKeshvari.Rows[i][j].ToString());
                                    Data[4] = "";
                                }
                                break;
                            case 5:
                                if (i == 0)
                                    Data[5] = "درصد";
                                else
                                {
                                    if (dtKeshvari.Rows[i][j].ToString() == "0")
                                        F = int.Parse(dtKeshvari.Rows[i][j].ToString());
                                    else
                                        F = int.Parse(dtKeshvari.Rows[i][j].ToString().Substring(0, 2));
                                    Data[5] = "(((G3*F3)+E3)/D3)*100";
                                }
                                break;
                            case 6:
                                if (i == 0)
                                    Data[6] = "قیمت بیمه";
                                else
                                    G = int.Parse(dtKeshvari.Rows[i][j].ToString());
                                break;
                            case 7:
                                if (i == 0)
                                    Data[7] = "/";
                                else
                                    Data[7] = "/";
                                break;
                            case 8:
                                if (i == 0)
                                    Data[8] = "حداقل قیمت";
                                else
                                    Data[8] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 9:
                                if (i == 0)
                                    Data[9] = "دفتر اسناد";
                                else
                                    Data[9] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 10:
                                if (i == 0)
                                    Data[10] = "پرونده ای";
                                else
                                    Data[10] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 11:
                                if (i == 0)
                                    Data[11] = "اخذ مدرک";
                                else
                                    Data[11] = "خير";
                                break;
                            case 12:
                                if (i == 0)
                                    Data[12] = "بارکد اصالت";
                                else
                                {
                                    Data[3] = dtKeshvari.Rows[i][j].ToString();
                                }
                                break;
                            case 13:
                                if (i == 0)
                                    Data[13] = "بیمارستانی";
                                else
                                    Data[13] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 14:
                                if (i == 0)
                                    Data[14] = "بیمه ای";
                                else
                                    Data[14] = dtKeshvari.Rows[i][j].ToString();
                                break;
                            case 15:
                                if (i == 0)
                                    Data[15] = "تاریخ اعمال";
                                else
                                    Data[15] = today;
                                break;
                            case 16:
                                if (i == 0)
                                    Data[16] = "تاریخ تبیان";
                                break;
                            case 17:
                                if (i == 0)
                                    Data[17] = "توضیحات";
                                break;
                        }
                    }
                    percent = (float)((G * F / 100) + E) / D * 100;

                    string strPercent = Data[5] == "درصد" ? "درصد" : (percent.ToString() != "NaN" ? percent.ToString() : "0");
                    if (i >= 0)
                        dtMain.Rows.Add(Data[0], Data[1], Data[2], Data[3], Data[4], strPercent, Data[6], Data[7], Data[8], Data[9], Data[10], Data[11], Data[12], Data[13], Data[14], Data[15], Data[16], Data[17]);
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dtMain);
                DgShowMergedData.ItemsSource = ds.Tables[0].DefaultView;
            }
            else
            {
                MessageBox.Show("لطفا ابتدا فایل‌های اکسل را وارد نمائید!");
            }
        }

        private void AddGridColumns(DataTable dataTable)
        {
            dataTable.Columns.Add("نام دارو");
            dataTable.Columns.Add("کد دارو");
            dataTable.Columns.Add("صرفا وبی");
            dataTable.Columns.Add("سقف تجویز");
            dataTable.Columns.Add("نیاز به اخذ مدرک");
            dataTable.Columns.Add("درصد");
            dataTable.Columns.Add("قیمت بیمه");
            dataTable.Columns.Add("اسلش");
            dataTable.Columns.Add("حداقل قیمت");
            dataTable.Columns.Add("دفتر اسناد");
            dataTable.Columns.Add("پرونده ای");
            dataTable.Columns.Add("اخذ مدرک");
            dataTable.Columns.Add("بارکد اصالت");
            dataTable.Columns.Add("بیمارستانی");
            dataTable.Columns.Add("بیمه ای");
            dataTable.Columns.Add("تاریخ اعمال");
            dataTable.Columns.Add("تاریخ تبیان");
            dataTable.Columns.Add("توضیحات");
        }

        public string Browse()
        {

            OpenFileDialog openFile = new OpenFileDialog() { Filter = "Excel Workbook|*.*", ValidateNames = true };
            string fileName = openFile.FileName;

            if (openFile.ShowDialog() == true)
            {
                path = openFile.FileName;
                FilePath.Text += path + "\n";
                return path;
            }
            else
            {
                return "null";
            }
        }
    }
}
