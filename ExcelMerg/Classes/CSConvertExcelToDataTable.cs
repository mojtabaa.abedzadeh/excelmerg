﻿#region Assembly Microsoft.Office.Interop.Excel, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c
// C:\Program Files (x86)\Microsoft Visual Studio\Shared\Visual Studio Tools for Office\PIA\Office15\Microsoft.Office.Interop.Excel.dll
// Decompiled with ICSharpCode.Decompiler 7.1.0.6543
#endregion

using System;
using System.IO;
using System.Data.OleDb;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace ExcelMerg.Classes
{
    public static class CSConvertExcelToDataTable
    {
        public static System.Data.DataTable GetDataTable(string path, bool isHDR = true)
        {
            var hdr = isHDR ? "YES" : "No";
            string connectionString;
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".xls":
                case ".XLS":
                    connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties='Excel 8.0;HDR={hdr}'";
                    break;
                case ".xlsx":
                case ".XLSX":
                    connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties='Excel 8.0;IMEX=1 ;HDR={hdr}'";
                    break;
                default:
                    throw new Exception($"Not supported file extension: `{ext}`");
            }
            var dataTable = new System.Data.DataTable();

            using (var connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                var schema = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                using (var adaptor = new OleDbDataAdapter($"SELECT * FROM [{schema.Rows[0]["TABLE_NAME"]}]", connection))
                {
                    adaptor.Fill(dataTable);
                }
            }
            return dataTable;
        }

        public static void ExportToExcel(this System.Data.DataTable dataTable, String filePath, bool overwiteFile = true)
        {
            if (File.Exists(filePath) && overwiteFile)
            {
                File.Delete(filePath);
            }
            var conn = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={filePath};Extended Properties='Excel 8.0;HDR=Yes;IMEX=0';";
            using (OleDbConnection connection = new OleDbConnection(conn))
            {
                connection.Open();
                using (OleDbCommand command = new OleDbCommand())
                {
                    command.Connection = connection;

                    List<String> columnNames = new List<string>();
                    foreach (DataColumn dataColumn in dataTable.Columns)
                    {
                        columnNames.Add(dataColumn.ColumnName);
                    }

                    String tableName = !String.IsNullOrWhiteSpace(dataTable.TableName) ? dataTable.TableName : Guid.NewGuid().ToString();
                    command.CommandText = $"CREATE TABLE [{tableName}] ({String.Join(",", columnNames.Select(c => $"[{c}] VARCHAR").ToArray())});";
                    command.ExecuteNonQuery();


                    foreach (DataRow row in dataTable.Rows)
                    {
                        List<String> rowValues = new List<string>();

                        foreach (DataColumn column in dataTable.Columns)
                        {
                            if (row[column].ToString().Contains('\''))
                                row[column] = row[column].ToString().Contains('\'').ToString().Replace("'", " ");

                            rowValues.Add((row[column] != null && row[column] != DBNull.Value) ? row[column].ToString() : String.Empty);
                        }
                        command.CommandText = $"INSERT INTO [{tableName}]({String.Join(",", columnNames.Select(c => $"[{c}]"))}) VALUES ({String.Join(",", rowValues.Select(r => $"'{r}'").ToArray())});";
                        command.ExecuteNonQuery();
                    }
                }

                connection.Close();
            }
        }
    }
}
